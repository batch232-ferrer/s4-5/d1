package com.zuitt.example;

public class Dog extends Animal {
    // [PROPERTIES]
    // --> extends - keyword used to inherit the properties of a class.
    private String breed;

    // [CONSTRUCTORS]
    public Dog(){
        super(); // --> calls the Animal() constructor
        this.breed = "Golden Retriever";
    }

    public Dog(String name, String color, String breed){
        super(name, color); // --> calls the parameterized Animal() constructor
        this.breed = breed;
    }

    // [GETTERS/SETTERS]
    // --> [Getter]
    public String getBreed(){
        return this.breed;
    }

    // --> [Setter]
    public void setDogBreed(String dogBreed){
        this.breed = breed;
    }

    // [METHOD]
    public void speak(){
        super.call(); // --> calls the call() method from Animal Class
        System.out.println("Woof!");
    }
}
