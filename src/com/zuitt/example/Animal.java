package com.zuitt.example;

public class Animal {
    // [PROPERTIES]
    private String name;
    private String color;

    // [CONSTRUCTORS]
    public Animal(){};

    // --> needs setters since it requires values.
    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }

    // [GETTERS/SETTERS]
    // --> [Getters]
    public String getName(){
        return name;
    }
    public String getColor(){
        return color;
    }

    // --> [Setters]
    public void setName(String name){
        this.name = name;
    }
    public void setColor(String color){
        this.color = color;
    }

    // [METHODS]
    public void call(){
        System.out.println("Hi my name is " + this.name);
    }
}
